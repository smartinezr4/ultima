package net.infobosccoma.appgrupstreball.view;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import net.infobosccoma.appgrupstreball.model.Estudiant;
import net.infobosccoma.appgrupstreball.model.GrupTreball;

/**
 * Classe que conté el mètode principal de l'aplicació de gestió d'estudiants i
 * grups de treball.
 *
 * L'aplicació ha d gestionar, mitjançant una BDOO db4o, a quins grups de
 * treball pertanyen diferents estudiants dins una escola. Donat un grup de
 * treball, aquest pot tenir assignats diversos estudiants, però tot estudiant
 * només té un (però sempre un) únic grup de treball. L’aplicació ha de poder
 * fer el següent: - Donar d’alta un nou estudiant. A l’hora d’assignar-li un
 * grup, si el nom indicat no existeix, es crea un nou grup. Si existeix, a
 * l’estudiant se li assigna aquell grup. - Reassignar un estudiant a un altre
 * grup de treball. Aquest grup ja ha d’existir. Si, en fer-ho, el grup antic
 * queda sense membres, cal esborrar-lo de la BDOO. - Llistar tots els grups
 * existents. - Llistar tots els estudiants (i a quin grup pertanyen).
 *
 * Es considera que els noms dels grups i dels estudiants són únics al sistema.
 * No hi pot haver noms repetits. En base a la descripció, també cal remarcar
 * que l’única manera de crear grups nous és afegint-hi nous estudiants.
 *
 * @author Sergi
 */
public class AppGrupsTreball {

    private static Scanner scan;

    public static void main(String[] args) throws Exception {
        scan = new Scanner(System.in);
        ObjectContainer db = Db4oEmbedded.openFile("Estudiants.db4o");

        try {

            showMenu();
            int o = scan.nextInt();
            scan.nextLine();

            switch (o) {
                case 1:
                    //Afegir estudiant
                    System.out.println("Introdueix el nom del nou estudiant: ");
                    String estudiantNom = scan.nextLine();
                    System.out.print("Introdueix el nom del seu grup: ");
                    String estudiantGrupNom = scan.nextLine();
                    System.out.println("Introdueix el tema del grup: ");
                    String estudiantGrupTema = scan.nextLine();

                    inserirEstudiant(estudiantNom, estudiantGrupNom, estudiantGrupTema, db);
                    break;
                case 2:
                    //Reassignar grup d'estudiant
                    System.out.println("Introdueix el nom de l'estudiant: ");
                    String estudiantNom2 = scan.nextLine();
                    System.out.print("Introdueix el nom del seu grup actual: ");
                    String grupAntic = scan.nextLine();
                    System.out.println("Introdueix el nom del grup al que canviar-lo: ");
                    String grupNou = scan.nextLine();
                    reassignar(estudiantNom2, grupAntic, grupNou, db);
                    break;
                case 3:
                    //Llista grups
                    llistarGrups(db);
                    break;
                case 4:
                    //Llista estudiants
                    llistarEstudiants(db);
                    break;
            }
            
        } finally {
            db.close();
        }

    }

    
    /**
     * @param nom
     * @param grupNom
     * @param grupTema
     * @param db
     *
     * Metode per inserir un nou estudiant llegint parametre per teclat
     */
    static void inserirEstudiant(String nom, String grupNom, String grupTema, ObjectContainer db) {

        GrupTreball grup = new GrupTreball(grupNom, null);

        if (nom.equals("") || grupNom.equals("") || grupTema.equals("")) {

            System.out.println("Introdueix valors per tots els camps.");

        } else {

            comprovarGrup(grup, grupNom, grupTema, db);
            Estudiant e = new Estudiant(nom, grup);
            System.out.println("Nou estudiant: " + nom);
            db.store(e);

        }
    }

    private static void comprovarGrup(GrupTreball grup, String grupNom, String grupTema, ObjectContainer db) {

        ObjectSet<GrupTreball> result = db.queryByExample(grup);
        if (result.size() > 0) {
            System.out.println("Grup existeix, tot correcte.");
            grup = result.next();
            grup.sumaEstudiant();
            System.out.println("Total estudiants per el grup -> " + grupNom + ": " + grup.getNumEstudiants());
            db.store(grup);
        } else {
            GrupTreball grupNew = new GrupTreball(grupNom, grupTema);
            System.out.println("El grup indicat no existeix, creant-lo...");
            db.store(grupNew);
        }

    }

    private static void reassignar(String estudiantNom2, String grupAntic, String grupNou, ObjectContainer db) {

        ObjectSet<GrupTreball> result = db.queryByExample(grupNou);
        GrupTreball grupA = new GrupTreball(grupAntic, null);
        GrupTreball grupN = new GrupTreball(grupNou, null);

        if (estudiantNom2.equals("") || grupAntic.equals("") || grupNou.equals("")) {

            System.out.println("Introdueix valors per tots els camps.");

        } else if (result.size() > 0) {
            grupA = result.next();
            grupA.restaEstudiant();
            //estudiantNom2.reassignaGrup(grupNou);

            if (grupA.getNumEstudiants() == 0) {
                // Eliminar grup si es queda buit
                db.delete(grupA);
            }
            // Assignar nou grup a estudian            
            Predicate query = new Predicate<Estudiant>() {
                @Override
                public boolean match(Estudiant e) {
                    //Codi pel criteri de cerca
                    return estudiantNom2.equalsIgnoreCase(e.getNom());
                }
            };
            ObjectSet<Estudiant> resultNouGrup = db.query(query);
            if (result.size() != 1) {
                System.out.println("Estudiant no trobat");
            } else {
                Estudiant e = resultNouGrup.next();
                e.reassignaGrup(grupN);
                //db.delete(p);
            }

        } else {
            System.out.println("El grup nou introduit no existeix.");
        }
    }

    private static void llistarGrups(ObjectContainer db) {
        Predicate query = new Predicate<GrupTreball>() {
            @Override
            public boolean match(GrupTreball g) {
                return true;
            }
        };

        ObjectSet<GrupTreball> result = db.query(query);
        if (result.isEmpty()) {
            System.out.println("No s'ha trobat cap grup");
        } else {
            while (result.hasNext()) {
                GrupTreball g = result.next();
                System.out.println(g);
            }
        }
    }

    private static void llistarEstudiants(ObjectContainer db) {
        Predicate query = new Predicate<Estudiant>() {
            @Override
            public boolean match(Estudiant e) {
                return true;
            }
        };

        ObjectSet<Estudiant> result = db.query(query);
        if (result.isEmpty()) {
            System.out.println("No s'ha trobat cap estudiant");
        } else {
            while (result.hasNext()) {
                Estudiant e = result.next();
                System.out.println(e);
            }
        }
    }

    private static void showMenu() {
        System.out.println("Gestio d'estudiants");
        System.out.println("--------------------");
        System.out.println("1. Afegir Estudiants");
        System.out.println("2. Reassignar Grups");
        System.out.println("3. Llistar Grups");
        System.out.println("4. Llistar Estudiants");
        System.out.println("0. Sortir");
    }
}
